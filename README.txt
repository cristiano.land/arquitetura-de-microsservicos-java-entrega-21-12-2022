

Aplicação é formada por três microserviços que entregam as funções do software, são eles: consumacao, checkin, client.
Ainda temos um serviço de configuração, chamado configserver, um serviço de gateway chamado gateway e um serviço de server discovery, chamado de eureca.

Os serviços devem ser iniciados na seguinte sequência: 
1º - eureca
2º - configserver
3º - gateway
4º - checkin
5º - consumacao

Para criar um cliente deve fazer a chamada POST http://localhost:8180/clients/ e enviar os seguintes atributos:

{
    "email": "cliente1@teste.com",
    "nome": "Cliente 1",
    "endereco": "Rua X, 123"
}

para fazer retornar todos os clientes deve chamar o GET http://localhost:8180/clients/

para retornar um cliente código 25 em específico deve chamar o GET http://localhost:8180/clients/25

para deletar um cliente código 25 em específico deve chamar o DELETE http://localhost:8180/clients/25

