package br.edu.infnet.order;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import br.edu.infnet.order.Resource.dto.CheckinDTO;
import br.edu.infnet.order.Resource.dto.CheckinResponseDTO;
import br.edu.infnet.order.Resource.dto.ClientDTO;
import br.edu.infnet.order.Resource.dto.ConsumacaoDTO;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CheckinApplicationTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void createCheckinTest() throws Exception {

        // Cria um cliente para associar ao check-in
        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setCodigo(1L);
        clientDTO.setEmail("cliente1@empresa.com");
        clientDTO.setNome("Cliente 1");
        clientDTO.setEndereco("Rua do Cliente, 123");

        // Cria uma lista de consumações para associar ao check-in
        ConsumacaoDTO consumacaoDTO1 = new ConsumacaoDTO();
        consumacaoDTO1.setId(1);
        consumacaoDTO1.setNumero(1);
        consumacaoDTO1.setValor(100);
        consumacaoDTO1.setDescricao("Consumação 1");

        ConsumacaoDTO consumacaoDTO2 = new ConsumacaoDTO();
        consumacaoDTO2.setId(2);
        consumacaoDTO2.setNumero(2);
        consumacaoDTO2.setValor(50);
        consumacaoDTO2.setDescricao("Consumação 2");

        List<ConsumacaoDTO> consumacoes = new ArrayList<>();
        consumacoes.add(consumacaoDTO1);
        consumacoes.add(consumacaoDTO2);

        // Cria o request DTO com as informações do check-in
        CheckinDTO  requestDTO = new CheckinDTO();
        //requestDTO.setClientID(clientDTO);
        requestDTO.setClientID(1);
        requestDTO.setConsumacoes(consumacoes);

        // Configura o cabeçalho da requisição
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        // Converte o request DTO para JSON
        ObjectMapper objectMapper = new ObjectMapper();
        String requestJson = objectMapper.writeValueAsString(requestDTO);

        // Cria a entidade HTTP com o request JSON e o cabeçalho configurado
        HttpEntity<String> requestEntity = new HttpEntity<>(requestJson, headers);

        // Faz a chamada POST para a criação do check-in
        CheckinResponseDTO responseDTO = restTemplate.exchange("/checkin", HttpMethod.POST, requestEntity, CheckinResponseDTO.class).getBody();

        // Verifica se a resposta não é nula
        assertNotNull(responseDTO);

        // Verifica se o ID do check-in foi gerado
        assertNotNull(responseDTO.getCheckinId());

        // Faz a chamada GET para obter o check-in criado
        CheckinResponseDTO createdCheckin = restTemplate.getForObject("/checkin/" + responseDTO.getCheckinId(), CheckinResponseDTO.class);

        // Verifica se o ID do check-in retornado corresponde ao ID do check-in criado
        assertEquals(responseDTO.getCheckinId(), createdCheckin.getCheckinId());

        // Verifica se o nome do cliente retornado corresponde ao nome do cliente criado
        assertEquals(clientDTO.getNome(), createdCheckin.getClientDTO().getNome());

        // Verifica se o endereço do cliente retornado corresponde ao endereço do cliente criado
        assertEquals(clientDTO.getEndereco(), createdCheckin.getClientDTO().getEndereco());

        // Verifica se a lista de consumações retornada corresponde à lista de consumações criada
        assertEquals(requestDTO.getConsumacoes(), createdCheckin.getConsumacoes());
    }
}