package br.edu.infnet.consumacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ConsumacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumacaoApplication.class, args);
	}

}
