package br.edu.infnet.consumacao.model.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Consumacao {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private int numero;
	private int valor;
	private String descricao;

	public Consumacao() {
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Consumacao(Integer id, int numero, int valor, String descricao) {
		super();
		this.id = id;
		this.numero = numero;
		this.valor = valor;
		this.descricao = descricao;
	}
	@Override
	public String toString() {
		return "Consumacao [id=" + id + ", numero=" + numero + ", valor=" + valor + ", descricao=" + descricao + "]";
	}
	
}
