package br.edu.infnet.client.model.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.edu.infnet.client.model.entities.Client;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {

	Optional<Client> findById(Long codigo);

	
}
