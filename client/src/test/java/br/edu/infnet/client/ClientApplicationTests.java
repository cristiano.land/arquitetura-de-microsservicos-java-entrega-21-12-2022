package br.edu.infnet.client;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import br.edu.infnet.client.model.entities.Client;
import br.edu.infnet.client.model.services.ClientService;

@SpringBootTest
class ClientApplicationTests {

    @Autowired
    private ClientService clientService;
    
    @Test
    void testCreateAndDeleteClient() {
        // Criação do novo cliente
        Client newClient = new Client();
        newClient.setEmail("cliente1@teste.com");
        newClient.setNome("Cliente 1");
        newClient.setEndereco("Rua X, 123");
        
        // Salvar o novo cliente
        Client savedClient = clientService.save(newClient);
        
        // Verificar se o cliente foi criado com sucesso
        Client foundClient = clientService.getByCodigo(savedClient.getCodigo());
        assertNotNull(foundClient);
        
        // Deletar o cliente criado
        clientService.deleteByCodigo(savedClient.getCodigo());
        
		/*
		 * // Verificar se o cliente foi deletado com sucesso foundClient =
		 * clientService.getByCodigo(savedClient.getCodigo()); assertNull(foundClient);
		 */
    }

}
