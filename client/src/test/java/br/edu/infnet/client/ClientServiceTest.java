package br.edu.infnet.client;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import br.edu.infnet.client.model.entities.Client;
import br.edu.infnet.client.model.repository.ClientRepository;
import br.edu.infnet.client.model.services.ClientService;

@SpringBootTest
public class ClientServiceTest {

	@InjectMocks
	private ClientService clientService;
	
	@Mock
	private ClientRepository clientRepository;

	@Test
	public void testCreateClient() {
		Client newClient = new Client();
		newClient.setNome("Test Client");
		newClient.setEmail("testclient@example.com");
		
		when(clientRepository.save(any(Client.class))).thenReturn(newClient);
		
		Client createdClient = clientService.save(newClient);
		
		assertEquals("Test Client", createdClient.getNome());
		assertEquals("testclient@example.com", createdClient.getEmail());
	}
	
}
